# snowflake hosting Vagrant configuration

require 'yaml'
path = "#{File.dirname(__FILE__)}"

# Get machine configuration
configuration = {}
sitename = ''
if File.exist?(path + '/cnf/vagrant-puppet.yaml')
  data = YAML::load(File.read(path + '/cnf/vagrant-puppet.yaml')) || {}

  # Get machine configuration
  configuration = data['vagrantconf'] || {}

  # Get Website config
  if data.has_key?('website::sites')
    sitename = data['website::sites'].keys.first.to_s || nil
  end
end

# OS detection
module OS
  def OS.windows?
    (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
  end
  def OS.mac?
    (/darwin/ =~ RUBY_PLATFORM) != nil
  end
  def OS.linux?
    !OS.windows? and !OS.mac?
  end
end

# set appropriate provider
if OS.windows?
  ENV['VAGRANT_DEFAULT_PROVIDER'] = 'virtualbox'
  syncedfolder_type = 'smb'
elsif OS.mac?
  ENV['VAGRANT_DEFAULT_PROVIDER'] = 'virtualbox'
  syncedfolder_type = 'nfs'
elsif OS.linux?
  ENV['VAGRANT_DEFAULT_PROVIDER'] = 'lxc'
  syncedfolder_type = 'nfs'
end

# overwrite provider by config if set.
syncedfolder_type = configuration['syncedfolder_type'] || syncedfolder_type
ENV['VAGRANT_DEFAULT_PROVIDER'] = configuration['provider'] || ENV['VAGRANT_DEFAULT_PROVIDER']

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # Configuration
  name    = configuration['name'] || "box"
  domain  = configuration['domain'] || "vagrant"
  aliases = configuration['alias'] || ""
  memory  = configuration['memory'] || 1024
  cpus    = configuration['cpus'] || 1
  webuser = configuration['webuser'] || sitename || name.to_s + domain.to_s

  # LXC related settings
  config.vm.provider :lxc do |lxc, override|
    override.vm.box = "https://www.snowflakehosting.ch/vagrant/lxc-jessie-amd64.box"
    # allow start without AppArmor, otherwise Box will not Start on Ubuntu 14.10
    lxc.customize 'aa_allow_incomplete', 1
  end

  # VirtualBox related settings
  config.vm.provider "virtualbox" do |vbox, override|
    override.vm.box = "https://www.snowflakehosting.ch/vagrant/virtualbox-jessie-amd64.box"
    vbox.customize ["modifyvm", :id, "--memory", memory]
    vbox.customize ["modifyvm", :id, "--cpus",   cpus]
    vbox.name = "%s.#{domain}" % name.to_s
  end

  # vagrant-hostmanager
  if Vagrant.has_plugin?('vagrant-hostmanager')
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
  end

  # SSH options
  config.ssh.forward_agent = true

  config.vm.define name do |node|

    # vagrant-hostmanager
    if Vagrant.has_plugin?('vagrant-hostmanager')
      # set Aliases
      if defined? aliases
        node.hostmanager.aliases = name.to_s + " " + aliases.to_s
      end

      # Add custom IP resolver to vagrant-hostmanager (required for DHCP)
      node.hostmanager.ip_resolver = proc do |vm, resolving_vm|
        if hostname = (vm.ssh_info && vm.ssh_info[:host])
          if ENV['VAGRANT_DEFAULT_PROVIDER'] == 'virtualbox'
            `vagrant ssh -c "facter ipaddress_eth1" 2>&1`.split("\n").first[/(\d+\.\d+\.\d+\.\d+)/, 1]
          elsif ENV['VAGRANT_DEFAULT_PROVIDER'] == 'lxc'
            `vagrant ssh -c "facter ipaddress_eth0" 2>&1`.split("\n").first[/(\d+\.\d+\.\d+\.\d+)/, 1]
          end
        end
      end

      # Run the hostmanager plugin
      node.vm.provision :hostmanager
    end

    # private network, required for nfs
    if ENV['VAGRANT_DEFAULT_PROVIDER'] == 'virtualbox'
      node.vm.network "private_network", type: "dhcp"
    end

    # shared folders
    node.nfs.map_uid = Process.uid
    node.nfs.map_gid = Process.gid
    node.vm.synced_folder ".", "/vagrant" # don't dare to alter the type here, won't work
    if webuser.to_s.strip.length != 0
      node.vm.synced_folder ".", "/home/"+ webuser.to_s, type: syncedfolder_type, create: true
    end

    # set host name
    node.vm.hostname =  "%s.#{domain}" % name.to_s

    # update system and install dependencies
    node.vm.provision "shell", inline: "apt-get update -qq && apt-get upgrade -y"
    node.vm.provision "shell", inline: "apt-get install -y puppet git"
    node.vm.provision "shell", inline: "apt-get autoremove -y --purge"

    # Determine Source to get puppet-modules from (for testing- or debuging purposes mainly...)
    puppet_repo   = configuration['puppet_repo_path'] || "/h/puppet-modules.git"
    puppet_branch = configuration['puppet_branch'] || "201501_master" 
    # clone or update puppet modules
    node.vm.provision "shell", inline: "mkdir -p -m 0700 ~/.ssh/ && touch ~/.ssh/known_hosts && grep gate.snowflake.ch ~/.ssh/known_hosts > /dev/null || ssh-keyscan gate.snowflake.ch >> ~/.ssh/known_hosts 2>/dev/null"
    node.vm.provision "shell", inline: "cd /home/vagrant/puppet-modules/ 2> /dev/null && git fetch origin && git rebase --stat origin/" + puppet_branch.to_s + " || git clone --branch " + puppet_branch.to_s + " ssh://git@gate.snowflake.ch" + puppet_repo.to_s + " /home/vagrant/puppet-modules/"

    # configure hiera
    node.vm.provision "shell", inline: "echo -e '---\n:backends:\n  - yaml\n:yaml:\n  :datadir: /home/" + webuser.to_s + "/cnf/\n:hierarchy:\n  - vagrant-puppet' > /home/vagrant/hiera.yaml"

    # inline exec Puppet, without puppet provisioner which assumes that our Puppet modules are outside the box
    node.vm.provision "shell", inline: "FACTER_syncedfolder_type=" + syncedfolder_type.to_s + " FACTER_host_uid=" + node.nfs.map_uid.to_s + " FACTER_host_gid=" + node.nfs.map_gid.to_s + " FACTER_webuser=" + webuser.to_s + " FACTER_virtual_type=vagrant puppet apply --parser future --modulepath /home/vagrant/puppet-modules/modules/ --hiera_config=/home/vagrant/hiera.yaml --manifestdir /tmp/vagrant-puppet-3/manifests -e 'include base'"
  end
end

