"use strict";

var path    = require('path');
var gulp    = require('gulp');
var sass    = require('gulp-sass');
var concat  = require('gulp-concat');
var del     = require('del');


var bowerDir   = path.join(__dirname, './bower_components');
var customDir  = path.join(__dirname, './custom_components');
var publicDir  = path.join(__dirname, './www');


gulp.task('clean', function () {
    return del([
        path.join(publicDir, './**/*.*')
    ]);
});


gulp.task('robotofont', function () {
    gulp.src([
        path.join(bowerDir, "roboto-fontface/fonts/**")
    ])
        .pipe(gulp.dest(path.join(publicDir, "roboto/")));
});

gulp.task('fonts', function () {
    gulp.src([
        path.join(bowerDir, "materialize/font/**"),
    ])
        .pipe(gulp.dest(publicDir));
});


gulp.task('jslibs', function () {
    gulp.src([
        path.join(bowerDir, "jquery/dist/jquery.min.js"),
        path.join(bowerDir, "materialize/dist/js/**/*.js")
    ])
        .pipe(concat('libs.js'))
        .pipe(gulp.dest(publicDir));
});


gulp.task('jsstatic', function () {
    gulp.src(path.join(bowerDir, "materialize/templates/parallax-template/js/*.js"))
        .pipe(gulp.dest(publicDir));
});


gulp.task('downloads', function () {
    gulp.src([
        path.join(customDir, "downloads/**")
    ],
    {'base': customDir})
        .pipe(gulp.dest(publicDir));
});
gulp.task('images', function () {
    gulp.src([
        path.join(customDir, "design/**"),
        path.join(customDir, "images/**")
    ],
    {'base': customDir})
        .pipe(gulp.dest(publicDir));
});


gulp.task('index', function () {
    gulp.src([
        path.join(customDir, "index.php")
    ],
    {'base': customDir})
        .pipe(gulp.dest(publicDir));
});


gulp.task('robots', function () {
    gulp.src([
        path.join(customDir, "robots.txt")
    ],
    {'base': customDir})
        .pipe(gulp.dest(publicDir));
});


gulp.task('favicon', function () {
    gulp.src([
        path.join(customDir, "favicon.ico")
    ],
    {'base': customDir})
        .pipe(gulp.dest(publicDir));
});


gulp.task('sass', function () {
    gulp.src([
        path.join(customDir, 'styles/**/*.scss')
    ])
        .pipe(sass())
        .pipe(gulp.dest(publicDir));
});


gulp.task('default', ['clean', 'sass', 'jslibs', 'jsstatic', 'downloads','images', 'index', 'robots', 'favicon', 'fonts', 'robotofont']);
