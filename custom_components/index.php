<?php

$FLUID_CACHE_DIRECTORY = !isset($FLUID_CACHE_DIRECTORY) ? __DIR__ . '/../tmp/' : $FLUID_CACHE_DIRECTORY;

require __DIR__ . '/../vendor/autoload.php';


// set up paths object with arrays of paths with files
$paths = new \TYPO3\Fluid\View\TemplatePaths();
$paths->setTemplateRootPaths(array(__DIR__ . '/../custom_components/Templates/'));
$paths->setLayoutRootPaths(array(__DIR__ . '/../custom_components/Layouts/'));
$paths->setPartialRootPaths(array(__DIR__ . '/../custom_components/Partials/'));

// pass the constructed TemplatePaths instance to the View
$view = new \TYPO3\Fluid\View\TemplateView($paths);


if ($FLUID_CACHE_DIRECTORY) {
	// Configure View's caching to use ./examples/cache/ as caching directory.
	$view->setCache(new \TYPO3\Fluid\Core\Cache\SimpleFileCache($FLUID_CACHE_DIRECTORY));
}

$data = json_decode(file_get_contents('../data/content.json'), true);

$view->assignMultiple($data);

echo isset($_GET['gallery']) ? $view->render('Gallery') : $view->render('Default') . PHP_EOL . PHP_EOL;
